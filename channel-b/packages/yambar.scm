;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (channel-b packages yambar)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages datastructures)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages man)
  #:use-module (gnu packages mpd)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages web)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xorg)
  #:use-module (guix build-system meson)
  #:use-module (guix build utils)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  )

(define yambar-default-dependencies
  (list
   eudev
   fcft
   libyaml
   pipewire
   pixman
   pulseaudio
   tllist
   ;; optional dependencies
   alsa-lib
   json-c
   libmpdclient
   ))

(define yambar-xorg-dependencies
  (list
   xcb-util
   xcb-util-cursor
   xcb-util-errors
   xcb-util-wm
   ))

(define yambar-wayland-dependencies
  (list
   wayland
   wayland-protocols
   ))

(define yambar-modified-phases
  #~(modify-phases %standard-phases
      (add-before 'check 'set-environment
        (lambda* (#:key outputs #:allow-other-keys)
          (setenv "XDG_CACHE_HOME" ".cache")))))

(define-public yambar
  (package
    (name "yambar")
    (version "1.10.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://codeberg.org/dnkl/yambar")
             (commit "c4e094de3e531675bae9ff76d7e6f0dfdcaecae9")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "14lxhgyyia7sxyqjwa9skps0j9qlpqi8y7hvbsaidrwmy4857czr"))))
    (build-system meson-build-system)
    (arguments
     (list #:configure-flags
           #~(list
              "--buildtype=release"
              "--wrap-mode=nofallback"
              "-Db_lto=true"
              "-Dbackend-x11=enabled"
              "-Dbackend-wayland=enabled"
              )
           #:phases yambar-modified-phases))
    (inputs (append yambar-default-dependencies
                    yambar-wayland-dependencies
                    yambar-xorg-dependencies))
    (native-inputs (list
                    bison
                    flex
                    pkg-config
                    scdoc
                    ))
    (home-page "https://codeberg.org/dnkl/yambar")
    (synopsis "Modular status panel for X11 and Wayland, inspired by polybar")
    (description "Simplistic and highly configurable status panel for Wayland
and X11.")
    (license license:expat)))

(define-public yambar-wayland
  (package
    (inherit yambar)
    (name "yambar-wayland")
    (arguments
     (list #:configure-flags
           #~(list
              "--buildtype=release"
              "--wrap-mode=nofallback"
              "-Db_lto=true"
              "-Dbackend-x11=disabled"
              "-Dbackend-wayland=enabled"
              )
           #:phases yambar-modified-phases))
    (inputs (append yambar-default-dependencies
                    yambar-wayland-dependencies))
    (synopsis "Modular status panel for Wayland, inspired by polybar")
    (description "Simplistic and highly configurable status panel for
Wayland (No X11 support).")))

(define-public yambar-wayland-minimal
  (package
    (inherit yambar-wayland)
    (name "yambar-wayland-minimal")
    (arguments
     (list #:configure-flags
           #~(list
              "--buildtype=release"
              "--wrap-mode=nofallback"
              "-Db_lto=true"
              "-Dbackend-x11=disabled"
              "-Dbackend-wayland=enabled"
              ;; personal preferences, remove as needed
              "-Dplugin-backlight=disabled"
              "-Dplugin-cpu=disabled"
              "-Dplugin-disk-io=disabled"
              "-Dplugin-dwl=disabled"
              "-Dplugin-mem=disabled"
              "-Dplugin-mpd=disabled"
              "-Dplugin-i3=disabled"
              "-Dplugin-network=disabled"
              "-Dplugin-pipewire=disabled"
              "-Dplugin-sway-xkb=disabled"
              )
           ;; no testing; custom configure-flags will cause some test to fail
           #:tests? #f
           #:phases yambar-modified-phases))
    (inputs (cons*
             alsa-lib
             eudev
             fcft
             libyaml
             pixman
             pulseaudio
             yambar-wayland-dependencies))
    (description "Simplistic and highly configurable status panel for
Wayland (No X11 support). Compiled With many modules disables. Intended for
use with RiverWM and the modules 'pulseaudio', 'battery' and 'clock'")))
