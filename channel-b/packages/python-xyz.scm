;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (channel-b packages python-xyz)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu)
  #:use-module (gnu packages check)
  #:use-module (gnu packages game-development)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages swig)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  )

(define-public python-farama-notifications
  (package
    (name "python-farama-notifications")
    (version "0.0.4")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "Farama-Notifications" version))
       (sha256
        (base32 "067xbra8xicgk7p6wcv5s73k6dxz5r36d0iwf20cy523s7rfzz0k"))))
    (build-system pyproject-build-system)
    (home-page "https://github.com/Farama-Foundation/Farama-Notifications")
    (synopsis "Notifications for all Farama Foundation maintained libraries.")
    (description
     "Notifications for all Farama Foundation maintained libraries.")
    (license #f)))

(define-public python-gymnasium
  (package
    (name "python-gymnasium")
    (version "0.29.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "gymnasium" version))
       (sha256
        (base32 "1cab4wsnlsxn2z90qmymv8ppmsq8yq2amiqwid3r0xfbxx92flqs"))))
    (build-system pyproject-build-system)
    (arguments
     ;; FIXME: test fail due to importing local file not being found.
     '(#:tests? #f))
    (propagated-inputs (list python-cloudpickle python-farama-notifications
                             python-importlib-metadata python-numpy
                             python-typing-extensions))
    (native-inputs (list python-pytest python-scipy))
    (home-page "")
    (synopsis
     "A standard API for reinforcement learning and a diverse set of reference environments (formerly Gym).")
    (description
     "This package provides a standard API for reinforcement learning and a diverse
set of reference environments (formerly Gym).")
    (license license:expat)))

(define-public python-pygame-2.5
  (package
    (inherit python-pygame)
    (name "python-pygame-2.5")
    (version "2.5.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pygame" version))
       (sha256
        (base32 "0jn2n70hmgr33yc6xzdi33cs5w7jnmgi44smyxfarrrrsnsrxf61"))))
    (build-system pyproject-build-system)))

(define-public python-box2d
  (package
    (name "python-box2d")
    (version "2.3.10")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/pybox2d/pybox2d")
             (commit "09643321fd363f0850087d1bde8af3f4afd82163")))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0zsrb1zf2vib9xckcir1cv3zc72ym85jz69s9byhnv0jpd9670kb"))))
    (build-system pyproject-build-system)
    (arguments
     '(#:tests? #f))                    ;no tests
    (propagated-inputs (list
                        swig
                        python-pygame-2.5 ;defined above
                        ))
    (home-page "http://www.box2d.org")
    (synopsis "Python 2D physics library")
    (description "Python 2D physics library")
    (license license:zlib)))
