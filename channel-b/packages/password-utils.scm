;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (channel-b packages password-utils)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages password-utils)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xdisorg)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix gexp)
  )

;; password-store but uses `bemenu` on wayland compositors instead of dmenu-wl
(define-public br-pass
  (package
    (inherit password-store)
    (name "br-pass")
    (arguments
     ;; I have no idea how this works but it allows modifying the phases of an
     ;; existing package. See `make-linux-xanmod` definition in
     ;; nongnu/packages/linux.scm from the nonguix channel
     (substitute-keyword-arguments (package-arguments password-store)
       ((#:phases phases)
        #~(modify-phases #$phases
            (add-before 'patch-passmenu-path 'new-paths
              (lambda* (#:key inputs #:allow-other-keys)
                (substitute* "contrib/dmenu/passmenu"
                  (("dmenu=dmenu-wl\n")
                   (string-append "dmenu="
                                  (search-input-file inputs "/bin/bemenu")
                                  "\n"))
                  (("xdotool=\"ydotool")
                   (string-append "xdotool=\""
                                  (search-input-file inputs "/bin/ydotool"))))))))))
    (inputs (modify-inputs (package-inputs password-store)
              (prepend bemenu ydotool)))))
