;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (channel-b packages rust-apps)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu)
  #:use-module (gnu packages base)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages man)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages polkit)
  #:use-module (gnu packages shells)
  #:use-module (guix build-system cargo)
  #:use-module (guix build-system copy)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (channel-b packages crates-io)
  )

(define-public swhkd
  (package
    (name "swhkd")
    (version "1.2.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/waycrate/swhkd")
             (commit "7628ad2d9030287f1590e087b892f96ef3794333") ; 1.2.1 release
             ;; (commit "30f25b5bf99df5f16d91b51a7bd397c1de075085") ;current main <2024-01-24 Wed>
             ))

       (file-name (git-file-name name version))
       (sha256
        (base32 "180nmzljxdw9sp2rmc214r7kpl0ib6504jwzqza15ili7pbb81am"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs
       (("rust-clap" ,rust-clap-3)
        ("rust-env_logger" ,rust-env-logger-0.9)
        ;; ("rust-evdev" ,rust-evdev-0.12)
        ("rust-flate2" ,rust-flate2-1)
        ("rust-itertools" ,rust-itertools-0.10)
        ("rust-log" ,rust-log-0.4)
        ("rust-nix" ,rust-nix-0.23)
        ("rust-signal-hook" ,rust-signal-hook-0.3)
        ;; ("rust-sysinfo" ,rust-sysinfo-0.23)
        ("rust-tokio" ,rust-tokio-1)
        ("rust-tokio-stream" ,rust-tokio-stream-0.1)
        ;; inputs defined in channel-b
        ("rust-evdev" ,rust-evdev-0.11)
        ("rust-sysinfo" ,rust-sysinfo-0.23.5)
        ("rust-signal-hook-tokio" ,rust-signal-hook-tokio-0.3)
        ("rust-tokio-udev" ,rust-tokio-udev-0.9))
       #:cargo-build-flags
       '("--release")
       #:phases
       (modify-phases %standard-phases
         (add-after 'build 'run-build-polkit-policy-script
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (policy "com.github.swhkd.pkexec.policy"))
               ;; for newer version there is a script that can be executed
               ;; to create the polkit policy with:
               ;; (when (assoc-ref inputs "dash")
               ;;   (with-output-to-file policy
               ;;     (lambda _
               ;;       (invoke "dash" "scripts/build-polkit-policy.sh"
	       ;;               ;; (string-append
               ;;               "--policy-path=-"
               ;;               ;; out
               ;;               ;; "/share/polkit-1/actions"
               ;;               ;; policy)
	       ;;               (string-append "--swhkd-path=" out "/bin/swhkd")))))
               (substitute* policy
                 (("/usr") out))
               )))
         (add-after 'build 'doc
           (lambda* (#:key inputs #:allow-other-keys)
             (when (assoc-ref inputs "scdoc")
               (with-directory-excursion "docs"
                 (map (lambda (page)
                        (invoke "sh" "-c"
                                (string-append "scdoc <" page ".scd >" page)))
                      '("swhkd-keys.5" "swhkd.1" "swhkd.5" "swhks.1"))))))
         (add-after 'doc 'polkit-rules
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out"))
                   (policy-port (open-file "swhkd.rules" "w")))
               (format policy-port "\
/* Allow users in wheel group to use swhkd requiring root without authentication */
polkit.addRule(function(action, subject) {
    if ((action.id == \"com.github.swhkd.pkexec\" &&
        subject.isInGroup(\"wheel\")) {
        return polkit.Result.YES;
    }
});"
                       out)
               (close policy-port)
               #t)))
         (replace 'install              ;from rust-swc and exa
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin"))
                    (share (string-append out "/share"))
                    (man1  (string-append share "/man/man1"))
                    (man5  (string-append share "/man/man5"))
                    (actions (string-append share "/polkit-1/actions"))
                    (rules (string-append share "/polkit-1/rules.d"))
                    )
               (map (lambda (file-target-pair)
                      (let ((file (car file-target-pair))
                            (target (cadr file-target-pair)))
                        (when (file-exists? file)
                          (install-file file target))))
                    `(("target/release/swhkd" ,bin)
                      ("target/release/swhks" ,bin)
                      ("docs/swhkd-keys.5" ,man5)
                      ("docs/swhkd.1" ,man1)
                      ("docs/swhkd.5" ,man5)
                      ("docs/swhks.1" ,man1)
                      ("swhkd.rules" ,rules)
                      ("com.github.swhkd.pkexec.policy" ,actions)))))))))
    (native-inputs (list
                    scdoc
                    pkg-config
                    ))
    (propagated-inputs (list
                        eudev
                        interception-tools ;provides uinput
                        polkit
                        ))
    (home-page "https://github.com/waycrate/swhkd")
    (synopsis "Sxhkd clone for Wayland (works on TTY and X11 too)")
    (description "Display protocol-independent hotkey daemon with an
easy-to-use configuration system inspired by @code{sxhkd}.

It also attempts to be a drop-in replacement for sxhkd, meaning your sxhkd
config file is also compatible with swhkd.

Because swhkd can be used anywhere, the same swhkd config can be used across
Xorg or Wayland desktops, and you can even use swhkd in a TTY.")
    (license license:bsd-2)))
