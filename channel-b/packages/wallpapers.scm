;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (channel-b packages wallpapers)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system copy)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define-public gruvbox-wallpapers
  (let ((commit "ec6572891f5f15ba4105711bc8b2ce601a89295b"))
    (package
      (name "gruvbox-wallpapers")
      (version "0.1")
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/bryos/gruvbox-wallpapers.git")
               (commit commit)))
         (sha256
          (base32
           "1z1l9fr0l0ygi2bpw14m26dp0airl9vshfy7zzsdqnark68gdflx"))
         (file-name (git-file-name name version))))
      (build-system copy-build-system)
      (arguments
       `(#:install-plan
         `(("assets/" "share/wallpapers"))))
      (native-search-paths
       (list (search-path-specification
              (variable "WALLPAPER_DIR")
              (files '("share/wallpapers")))))
      (home-page "https://gitlab.com/bryos/wallpapers")
      (synopsis "Wallpaper pack that go well with a Gruvbox theme.")
      (description "Wallpaper pack.")
      (license
       ;; I have no idea what the licenses are of these wallpapers, as they
       ;; were downloaded from random places of the internet; I will assume
       ;; they are in the public domain.
       license:public-domain))))
