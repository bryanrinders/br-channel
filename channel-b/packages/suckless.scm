;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (channel-b packages suckless)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages suckless)
  #:use-module (gnu packages xorg)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define-public br-dwm
  (let ((commit "f488a72c1cfedb738db0d7f53be41d263f2542ac"))
    (package
      (inherit dwm)
      (name "br-dwm")
      (version "6.4")
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/bryos/dwm")
               (commit commit)))
         (sha256
          (base32
           "035l2bb9rqnmk1hv0mcanb7ih6i852sbd3h037awgnfxhjf20v0m"))))
      (home-page "https://gitlab.com/bryos/dwm")
      (synopsis "Bryan's build of Suckless's DWM"))))

(define-public br-dmenu
  (let ((commit "2264e62e8434d43a2f138540307e860d93a9db41"))
    (package
      (inherit dmenu)
      (name "br-dmenu")
      (version "5.1.1")
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/bryos/dmenu")
               (commit commit)))
         (sha256
          (base32
           "1mzjzysdc69gsqyg7pwmj1y4yf6xhqhxrxs57vnsppkrxzrjc374"))))
      (home-page "https://gitlab.com/bryos/dwm")
      (synopsis "Bryan's build of Suckless's Dmenu"))))


(define-public br-st
  (let ((commit "c389473edd59d6eaff9eae8b199bc10323838d3d"))
    (package
      (inherit st)
      (name "br-st")
      (version "0.8.5")
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/bryos/st")
               (commit commit)))
         (sha256
          (base32
           "03ksbjh4xa60yq2mckaya3yazvnnrl8k1kf7g6w49yn831x7d2df"))))
      (home-page "https://gitlab.com/bryos/st")
      (synopsis "Bryan's build of Suckless's ST"))))

;; FIXME: patch-fix-pid-location has hardcoded the runtime_dir of user with id
;; 1000. Maybe use /tmp instead, but using the same filename for multi user
;; system will also be problematic. Should probably be fixed upstream. Rough
;; idea to set the LOCKFILE based on $XDG_RUNTIME_DIR:
;;
;; #include <stdlib.h>
;; #include <string.h>
;;
;; char *LOCKFILE;
;;
;; char *setLockfile() {
;;     char *pidFilename = "/dwmblocks.pid";
;;     char *xdg_runtime_dir = getenv("XDG_RUNTIME_DIR");
;;     int path_len = 1 + strlen(pidFilename);
;;
;;     if(xdg_runtime_dir == NULL) {
;;         xdg_runtime_dir = "/var/local/dwmblocks";
;;     }
;;     path_len += strlen(xdg_runtime_dir);
;;     LOCKFILE = (char*) malloc(path_len);
;;     strncat(strncat(LOCKFILE, xdg_runtime_dir, path_len), pidFilename, path_len);
;;     return 0;
;; }

(define-public br-dwmblocks
  (let ((commit "6e18027c433d8d07c6bf7ae94f17ab3b11c041b0"))
    (package
      (name "br-dwmblocks")
      (version "0.1")
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/bryos/dwmblocks")
               (commit commit)))
         (sha256
          (base32
           "1jpv67irszamjmxipz8bwa7x57zfbsfnnrgqqql1xsn8kfm3xima"))))
      (build-system gnu-build-system)
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           (delete 'configure)
           (add-before 'build 'patch-block-path
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (blocks (in-vicinity out "blocks")))
                 (substitute* "config.def.h"
                   (("<path to the folder containing block scripts>") blocks)))))
           (add-before 'build 'patch-fix-pid-location
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out")))
                 (substitute* "GNUmakefile"
                   ;; cannot make directories in /
                   (("	chmod 777 \\$\\{PIDDIR\\}") "")
                   (("	mkdir -p \\$\\{PIDDIR\\}") ""))
                 (substitute* "dwmblocks.c"
                   (("^#define LOCKFILE.*") "#define LOCKFILE \"/run/user/1000/dwmblocks.pid\"")))))
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (let ((out (assoc-ref outputs "out")))
                 (invoke "make" "install"
                         (string-append "DESTDIR=" out) "PREFIX="))))
           (add-after 'install 'install-blocks
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (blocks (in-vicinity out "blocks")))
                 (mkdir-p blocks)
                 (copy-recursively "blocks" blocks)))))
         #:tests? #f))
    (inputs (list libx11))
    (native-inputs (list pkg-config))
    (home-page "https://gitlab.com/bryos/dwmblocks")
    (synopsis "Bryan's build of Dwmblocks")
    (description "Modular status monitor for dwm written in C with features
including signaling, clickability, cursor hinting and color.")
    (license license:isc))))
