;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (channel-b packages hunspell)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system copy)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define-public hunspell-dict-nl
  (package
    (name "hunspell-dict-nl")
    (version "2.20.19")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/OpenTaal/opentaal-hunspell")
             (commit version)))
       (sha256
        (base32
         "0jma8mmrncyzd77kxliyngs4z6z4769g3nh0a7xn2pd4s5y2xdpy"))
       (file-name (git-file-name name version))))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan
       `(("." "share/hunspell" #:include ("nl.dic" "nl.aff")))))
    (home-page "https://www.opentaal.org/")
    (synopsis "Dutch dictionary for Hunspell")
    (description "Dutch dictionary for Hunspell.")
    (license license:bsd-3)))
