;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (channel-b packages fonts)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system font)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define-public font-all-nerd-fonts
  (package
    (name "font-all-nerd-fonts")
    (version "3.1.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/ryanoasis/nerd-fonts")
             (commit "23c0cbfaf80b8f6e04d69cf3a324c8a880b5a94f")))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0j89v2jznfdvrndb8z7rqw2pkmv58q5phyylh2pk9v930arxjg09"))))
    (build-system font-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'chdir
           (lambda* (#:key outputs #:allow-other-keys)
             (chdir "patched-fonts"))))))
    (home-page "https://www.nerdfonts.com/")
    (synopsis "All the @code{font-*-nerd} fonts")
    (description "Nerd Fonts patches developer targeted fonts with a high
number of glyphs (icons). Specifically to add a high number of extra glyphs
from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons, and
others.")
    (license license:silofl1.1)))

(define (string-kebab-case->camel str)
  "Convert a kebab-case string into a CamelCase string"
  (string-join (map string-capitalize (string-split str #\-)) ""))


(define (make-nerd-font name* version* hash)
  "NAME must be the 'kebab-case' version of the font directory as it can be
found in the repo."
  (package
    (inherit font-all-nerd-fonts)
    (name (string-append "font-" name* "-nerd"))
    (version version*)
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri (string-append
             "https://github.com/ryanoasis/nerd-fonts/releases/download/v"
             version "/" (string-kebab-case->camel name*) ".zip"))
       (sha256 (base32 hash))
       (file-name (git-file-name name version))))
    (arguments `(#:phases %standard-phases))
    (synopsis "Base font patched with extra glyphs")))

(define-public font-0x-proto-nerd
  (make-nerd-font
   "0x-proto" "3.1.1" "0gygxp9nrla7kxvbcj709iny1g0k167a49jb50lm2nmr6ipk2ayr"))

(define-public font-3270-nerd
  (make-nerd-font
   "3270" "3.1.1" "196b92p74514pg6x8i09p1r841k82ci06yikvpn8ld7r6q4jx832"))

(define-public font-agave-nerd
  (make-nerd-font
   "agave" "3.1.1" "0dphmf1fmrmp17dd4a43jj188agh3v6isfqys2ppb96bkl7fkrjf"))

(define-public font-anonymous-pro-nerd
  (make-nerd-font
   "anonymous-pro" "3.1.1" "088wpp8fzd06wfc3hq96h68mdsawncah6qbshdafzr0jndcfn5kb"))

(define-public font-arimo-nerd
  (make-nerd-font
   "arimo" "3.1.1" "0rf0bf3wgid3cgpa691awr16ig8x1ih784hxl8y7s38gbqzd4g13"))

(define-public font-aurulent-sans-mono-nerd
  (make-nerd-font
   "aurulent-sans-mono" "3.1.1" "1ryv587a9j15711a0qsvibpv3bp3qw0mrs8i411kgxny70rw0iz5"))

(define-public font-big-blue-terminal-nerd
  (make-nerd-font
   "big-blue-terminal" "3.1.1" "1ccdmp4gxmylf6ypwbm2a5mfd74fa5rmdwrrx4palcrfyap6pyrc"))

(define-public font-bitstream-vera-sans-mono-nerd
  (make-nerd-font
   "bitstream-vera-sans-mono" "3.1.1" "0q6akgn2pkla6z54rvdbk0p5n386y87jj5xv7spaffaabflic2c2"))

(define-public font-cascadia-code-nerd
  (make-nerd-font
   "cascadia-code" "3.1.1" "0rfq08jz5ysgp1k20nps8a773srvw1kzcwr2kgm1bhzaznkiy6ij"))

(define-public font-cascadia-mono-nerd
  (make-nerd-font
   "cascadia-mono" "3.1.1" "0mvipxc3f2yapa4cpms27jap8xnkvpacm1613qcisgxnphs5xlnw"))

(define-public font-code-new-roman-nerd
  (make-nerd-font
   "code-new-roman" "3.1.1" "0zxx7gj0z9c7i6ggza3n7jip0fwvp259s7d44ynrpp9n52amvpxg"))

(define-public font-comic-shanns-mono-nerd
  (make-nerd-font
   "comic-shanns-mono" "3.1.1" "09vk08df7c54yqbm49xawpgdyxvhg7lvcnxpng90251mr1zy2shn"))

(define-public font-commit-mono-nerd
  (make-nerd-font
   "commit-mono" "3.1.1" "0ci64628fpimb3sda2a9hi2g4ly15vkhgb9a897q8y560sv395ay"))

(define-public font-cousine-nerd
  (make-nerd-font
   "cousine" "3.1.1" "07vqa0v1pf74mcl0548v961kp99663w1isils25mw0h0b5l3wpzh"))

(define-public font-d2coding-nerd
  (make-nerd-font
   "d2coding" "3.1.1" "058hjiqi9vixnz4qfvgnbv34mvj9x4gahssslirhql180y642732"))

(define-public font-daddy-time-mono-nerd
  (make-nerd-font
   "daddy-time-mono" "3.1.1" "14prdmbzfzjnb0xdhxa767i0h3pfphxa26vpx29byi1ibybwxadv"))

(define-public font-deja-vu-sans-mono-nerd
  (make-nerd-font
   "deja-vu-sans-mono" "3.1.1" "09p5svrfxz28p2rzrq8g4jjh88k960la6swbl3xabr3ydfsnyv7l"))

(define-public font-droid-sans-mono-nerd
  (make-nerd-font
   "droid-sans-mono" "3.1.1" "119l97pgq1qki6v45aqnb6sa1648w6c47nd2c98gmlz8m48sbi4z"))

(define-public font-envy-code-r-nerd
  (make-nerd-font
   "envy-code-r" "3.1.1" "13ir7lcy0sr7yjnpmbp8pvxd1779yh6aivsq0v2fic5rlqkvzzhn"))

(define-public font-fantasque-sans-mono-nerd
  (make-nerd-font
   "fantasque-sans-mono" "3.1.1" "1r605g2zhyfx8jpdah6mdy5ran7yc3pnc9bbyhw0wbcdqp04j060"))

(define-public font-fira-code-nerd
  (make-nerd-font
   "fira-code" "3.1.1" "02b2p0gk0y998arisgknl7vmgyhxf2kfw19a3sxgfvqqbk67dmqs"))

(define-public font-fira-mono-nerd
  (make-nerd-font
   "fira-mono" "3.1.1" "0n3vs6wwc3zi6c1gkyfrllvh31ndqcycr33pj9k1ng53pmky9chn"))

(define-public font-geist-mono-nerd
  (make-nerd-font
   "geist-mono" "3.1.1" "14xpy3f141gr14x2s33113i2hs6jk576digyp86an0byp48j0432"))

(define-public font-go-mono-nerd
  (make-nerd-font
   "go-mono" "3.1.1" "05in5h2rm6w18wljzp8j8jxki6ds7ihdjqfg4k930ib77v4l1165"))

(define-public font-gohu-nerd
  (make-nerd-font
   "gohu" "3.1.1" "094ixg0icihxni6si1vpjbngw7rm5dx4skwdl1fraghv3kh2pfwq"))

(define-public font-hack-nerd
  (make-nerd-font
   "hack" "3.1.1" "13h64hsd20s03r0khfq8sriky9n8a26jmcxqraa75fmfnpdbkzsw"))

(define-public font-hasklig-nerd
  (make-nerd-font
   "hasklig" "3.1.1" "1y9rshxsshhr365hxr1nplz1lvrccs6f4rl7q6qdl8f5ijwmv0m6"))

(define-public font-heavy-data-nerd
  (make-nerd-font
   "heavy-data" "3.1.1" "0pknm4zsimfq9nq9w1cs4yik94ghl79ml4y35a282cy5j668xjf0"))

(define-public font-hermit-nerd
  (make-nerd-font
   "hermit" "3.1.1" "010a0vv5rzv4y5ra92i1svzz422kzkwh09qcf288qaik8lxy9fzq"))

(define-public font-ibm-plex-mono-nerd
  (make-nerd-font
   "ibm-plex-mono" "3.1.1" "1273jfwmyjq4l5nd9wddlmrqsjzzmab8mpa62l2aq3vnzb9gm654"))

(define-public font-inconsolata-nerd
  (make-nerd-font
   "inconsolata" "3.1.1" "1cdpvq9mm21bmyk0jk6vp1jrha55pgv4rw4x9gxxjhkv71qg9s5c"))

(define-public font-inconsolata-go-nerd
  (make-nerd-font
   "inconsolata-go" "3.1.1" "160nb77c6xsn92z9hmkl8q0i2c3wcsz9zpzgqbixnp0rjnxc5yxv"))

(define-public font-inconsolata-lgc-nerd
  (make-nerd-font
   "inconsolata-lgc" "3.1.1" "17n1l8q2rnbaj0absmz20i0k45vff97xydffz7g5z7kd330108fk"))

(define-public font-intel-one-mono-nerd
  (make-nerd-font
   "intel-one-mono" "3.1.1" "082ibcdi0rhcp7dbg6f6ig4vma08a2pckd8hdjlgmawrpzn7yri4"))

(define-public font-iosevka-nerd
  (make-nerd-font
   "iosevka" "3.1.1" "0sa1kj5s0sy4zv34fkwlwkas3qnaw2hvp44c3lxjj9xgpdzi6pjf"))

(define-public font-iosevka-term-nerd
  (make-nerd-font
   "iosevka-term" "3.1.1" "05slgym3ljzyf9f5j47a6g3047gxwbk49pz41fhqkbfa411m5f5x"))

(define-public font-iosevka-term-slab-nerd
  (make-nerd-font
   "iosevka-term-slab" "3.1.1" "0nz7i28sfrab8zlj9m5kw2dhfwaxrdmjxppymcr3ggiq7i5msr9j"))

(define-public font-jet-brains-mono-nerd
  (make-nerd-font
   "jet-brains-mono" "3.1.1" "1ai1in2f4yb8xyks945d1h4p87jilyc6chzggry567dbmqd310b4"))

(define-public font-lekton-nerd
  (make-nerd-font
   "lekton" "3.1.1" "0kl6np3d081fg838d13m8z1zdrpm368smbpvcrh807vb5i1xlx0s"))

(define-public font-liberation-mono-nerd
  (make-nerd-font
   "liberation-mono" "3.1.1" "1kx2mwnr1xr4ql80a1irawvd4s5fwiyb19qbwrjxi9y28gyrpnqm"))

(define-public font-lilex-nerd
  (make-nerd-font
   "lilex" "3.1.1" "0fjcq7747aysdx1jazhnbn067cqv23x2gp8v5b5knf9lq4gdqkrk"))

(define-public font-m-plus-nerd
  (make-nerd-font
   "m-plus" "3.1.1" "1k3db349nmzyxj08k09n1k4xr4p2g30sybpvxbvrp2hdmwkndyxj"))

(define-public font-martian-mono-nerd
  (make-nerd-font
   "martian-mono" "3.1.1" "1ljxi9w2xikam5hj9m6ysvyyzygrrgyqf6mjzs1mxxiw78pq8liv"))

(define-public font-meslo-nerd
  (make-nerd-font
   "meslo" "3.1.1" "1xx2hwsf6wj5in1l25yizbmk5w5sixz75m3pn6q3ni29avhrc43w"))

(define-public font-monaspace-nerd
  (make-nerd-font
   "monaspace" "3.1.1" "1hmnm7hksw5ibf96g9ma40d18rszy0ia6znz27cm336ril6ngrdz"))

(define-public font-monofur-nerd
  (make-nerd-font
   "monofur" "3.1.1" "1admpfnq70s2kv41f3jz9zkcqqqsln20bzaczn8j1qwdzbxp6y25"))

(define-public font-monoid-nerd
  (make-nerd-font
   "monoid" "3.1.1" "0f7sy13axbdjqyp01339xp285j61xhmpdbl0nwgbgdr02dqh8drz"))

(define-public font-mononoki-nerd
  (make-nerd-font
   "mononoki" "3.1.1" "19adpiq793163zhpw3ffhn18jmqkzi7pz5kai650jb6xlnxpjdl7"))

(define-public font-nerd-fonts-symbols-only-nerd
  (make-nerd-font
   "nerd-fonts-symbols-only" "3.1.1" "0xka49qk3r5m3jgpk3fi9b7j593fflg287ck99k12p8czqz62r4c"))

(define-public font-noto-nerd
  (make-nerd-font
   "noto" "3.1.1" "0j7cl7qp0rvp3x64kj1w6rr94gg6b0yxwliynf25qh498pq3pa0l"))

(define-public font-open-dyslexic-nerd
  (make-nerd-font
   "open-dyslexic" "3.1.1" "0204ap8ybwlr1dqlwnnmxg51hyx1g7bm4w6kx6x1wjrsibpshjkd"))

(define-public font-overpass-nerd
  (make-nerd-font
   "overpass" "3.1.1" "1h4v6b406r9hrsfjy8bmc2r3yijbqgn8pcxv8ihx1v53izmr3vyj"))

(define-public font-pro-font-nerd
  (make-nerd-font
   "pro-font" "3.1.1" "193y7wqxxh6n2g0x2yygiipdla0a6iw7wd2kg0q2s8d48a89s3s6"))

(define-public font-proggy-clean-nerd
  (make-nerd-font
   "proggy-clean" "3.1.1" "1ihcyrnwc7njskhlfbi7xhzck8j508sdqdca8m4rdail3xpi3k6j"))

(define-public font-roboto-mono-nerd
  (make-nerd-font
   "roboto-mono" "3.1.1" "121rhk2nn24dwwmz9pqi77p6cx1gx0cz78x1fxn67f4ahfswyjp8"))

(define-public font-share-tech-mono-nerd
  (make-nerd-font
   "share-tech-mono" "3.1.1" "13xbrc4d6bc1wjaybvbhnlsiv874sbkrjc7ai7310bv8z5v9h51s"))

(define-public font-source-code-pro-nerd
  (make-nerd-font
   "source-code-pro" "3.1.1" "0b048l525pig3wj7yr86710233xfnd7gpaigc04530b3ah013qw5"))

(define-public font-space-mono-nerd
  (make-nerd-font
   "space-mono" "3.1.1" "04awas15c38vlzk6wzagv0p0sac4glfzzlp2jf8gbxdf11hcl1lg"))

(define-public font-terminus-nerd
  (make-nerd-font
   "terminus" "3.1.1" "00j8glrgdc5w53k51bl8gilncw166h8i5yk9cbl5kbkpmn1h6y4q"))

(define-public font-tinos-nerd
  (make-nerd-font
   "tinos" "3.1.1" "1zbv4lmv5chxd7ycvjv0d6fpddpll9r4b9w5jflrkdjlmyq74c5y"))

(define-public font-ubuntu-nerd
  (make-nerd-font
   "ubuntu" "3.1.1" "1v3vc2nsl3a1yam2qf4rpiyd6wgpyn30n3a1izqn8sk2rm62zd33"))

(define-public font-ubuntu-mono-nerd
  (make-nerd-font
   "ubuntu-mono" "3.1.1" "1fg9abf421x3cwknh0chil4477gas289xp9s54m72ng9vx09jyxg"))

(define-public font-victor-mono-nerd
  (make-nerd-font
   "victor-mono" "3.1.1" "1ba50lhji0safw48v6r1xcxlgg92jgq4dklx9dk0garsfkhq4cib"))

(define-public font-i-a-writer-nerd
  (make-nerd-font
   "i-a-writer" "3.1.1" "1yd9n558fd63q8317s8my3nqn7q598zwv631k4p3g83mp9wash9r"))

;; 0xProto 0gygxp9nrla7kxvbcj709iny1g0k167a49jb50lm2nmr6ipk2ayr
;; 3270 196b92p74514pg6x8i09p1r841k82ci06yikvpn8ld7r6q4jx832
;; Agave 0dphmf1fmrmp17dd4a43jj188agh3v6isfqys2ppb96bkl7fkrjf
;; AnonymousPro 088wpp8fzd06wfc3hq96h68mdsawncah6qbshdafzr0jndcfn5kb
;; Arimo 0rf0bf3wgid3cgpa691awr16ig8x1ih784hxl8y7s38gbqzd4g13
;; AurulentSansMono 1ryv587a9j15711a0qsvibpv3bp3qw0mrs8i411kgxny70rw0iz5
;; BigBlueTerminal 1ccdmp4gxmylf6ypwbm2a5mfd74fa5rmdwrrx4palcrfyap6pyrc
;; BitstreamVeraSansMono 0q6akgn2pkla6z54rvdbk0p5n386y87jj5xv7spaffaabflic2c2
;; CascadiaCode 0rfq08jz5ysgp1k20nps8a773srvw1kzcwr2kgm1bhzaznkiy6ij
;; CascadiaMono 0mvipxc3f2yapa4cpms27jap8xnkvpacm1613qcisgxnphs5xlnw
;; CodeNewRoman 0zxx7gj0z9c7i6ggza3n7jip0fwvp259s7d44ynrpp9n52amvpxg
;; ComicShannsMono 09vk08df7c54yqbm49xawpgdyxvhg7lvcnxpng90251mr1zy2shn
;; CommitMono 0ci64628fpimb3sda2a9hi2g4ly15vkhgb9a897q8y560sv395ay
;; Cousine 07vqa0v1pf74mcl0548v961kp99663w1isils25mw0h0b5l3wpzh
;; D2Coding 058hjiqi9vixnz4qfvgnbv34mvj9x4gahssslirhql180y642732
;; DaddyTimeMono 14prdmbzfzjnb0xdhxa767i0h3pfphxa26vpx29byi1ibybwxadv
;; DejaVuSansMono 09p5svrfxz28p2rzrq8g4jjh88k960la6swbl3xabr3ydfsnyv7l
;; DroidSansMono 119l97pgq1qki6v45aqnb6sa1648w6c47nd2c98gmlz8m48sbi4z
;; EnvyCodeR 13ir7lcy0sr7yjnpmbp8pvxd1779yh6aivsq0v2fic5rlqkvzzhn
;; FantasqueSansMono 1r605g2zhyfx8jpdah6mdy5ran7yc3pnc9bbyhw0wbcdqp04j060
;; FiraCode 02b2p0gk0y998arisgknl7vmgyhxf2kfw19a3sxgfvqqbk67dmqs
;; FiraMono 0n3vs6wwc3zi6c1gkyfrllvh31ndqcycr33pj9k1ng53pmky9chn
;; GeistMono 14xpy3f141gr14x2s33113i2hs6jk576digyp86an0byp48j0432
;; Go-Mono 05in5h2rm6w18wljzp8j8jxki6ds7ihdjqfg4k930ib77v4l1165
;; Gohu 094ixg0icihxni6si1vpjbngw7rm5dx4skwdl1fraghv3kh2pfwq
;; Hack 13h64hsd20s03r0khfq8sriky9n8a26jmcxqraa75fmfnpdbkzsw
;; Hasklig 1y9rshxsshhr365hxr1nplz1lvrccs6f4rl7q6qdl8f5ijwmv0m6
;; HeavyData 0pknm4zsimfq9nq9w1cs4yik94ghl79ml4y35a282cy5j668xjf0
;; Hermit 010a0vv5rzv4y5ra92i1svzz422kzkwh09qcf288qaik8lxy9fzq
;; IBMPlexMono 1273jfwmyjq4l5nd9wddlmrqsjzzmab8mpa62l2aq3vnzb9gm654
;; Inconsolata 1cdpvq9mm21bmyk0jk6vp1jrha55pgv4rw4x9gxxjhkv71qg9s5c
;; InconsolataGo 160nb77c6xsn92z9hmkl8q0i2c3wcsz9zpzgqbixnp0rjnxc5yxv
;; InconsolataLGC 17n1l8q2rnbaj0absmz20i0k45vff97xydffz7g5z7kd330108fk
;; IntelOneMono 082ibcdi0rhcp7dbg6f6ig4vma08a2pckd8hdjlgmawrpzn7yri4
;; Iosevka 0sa1kj5s0sy4zv34fkwlwkas3qnaw2hvp44c3lxjj9xgpdzi6pjf
;; IosevkaTerm 05slgym3ljzyf9f5j47a6g3047gxwbk49pz41fhqkbfa411m5f5x
;; IosevkaTermSlab 0nz7i28sfrab8zlj9m5kw2dhfwaxrdmjxppymcr3ggiq7i5msr9j
;; JetBrainsMono 1ai1in2f4yb8xyks945d1h4p87jilyc6chzggry567dbmqd310b4
;; Lekton 0kl6np3d081fg838d13m8z1zdrpm368smbpvcrh807vb5i1xlx0s
;; LiberationMono 1kx2mwnr1xr4ql80a1irawvd4s5fwiyb19qbwrjxi9y28gyrpnqm
;; Lilex 0fjcq7747aysdx1jazhnbn067cqv23x2gp8v5b5knf9lq4gdqkrk
;; MPlus 1k3db349nmzyxj08k09n1k4xr4p2g30sybpvxbvrp2hdmwkndyxj
;; MartianMono 1ljxi9w2xikam5hj9m6ysvyyzygrrgyqf6mjzs1mxxiw78pq8liv
;; Meslo 1xx2hwsf6wj5in1l25yizbmk5w5sixz75m3pn6q3ni29avhrc43w
;; Monaspace 1hmnm7hksw5ibf96g9ma40d18rszy0ia6znz27cm336ril6ngrdz
;; Monofur 1admpfnq70s2kv41f3jz9zkcqqqsln20bzaczn8j1qwdzbxp6y25
;; Monoid 0f7sy13axbdjqyp01339xp285j61xhmpdbl0nwgbgdr02dqh8drz
;; Mononoki 19adpiq793163zhpw3ffhn18jmqkzi7pz5kai650jb6xlnxpjdl7
;; NerdFontsSymbolsOnly 0xka49qk3r5m3jgpk3fi9b7j593fflg287ck99k12p8czqz62r4c
;; Noto 0j7cl7qp0rvp3x64kj1w6rr94gg6b0yxwliynf25qh498pq3pa0l
;; OpenDyslexic 0204ap8ybwlr1dqlwnnmxg51hyx1g7bm4w6kx6x1wjrsibpshjkd
;; Overpass 1h4v6b406r9hrsfjy8bmc2r3yijbqgn8pcxv8ihx1v53izmr3vyj
;; ProFont 193y7wqxxh6n2g0x2yygiipdla0a6iw7wd2kg0q2s8d48a89s3s6
;; ProggyClean 1ihcyrnwc7njskhlfbi7xhzck8j508sdqdca8m4rdail3xpi3k6j
;; RobotoMono 121rhk2nn24dwwmz9pqi77p6cx1gx0cz78x1fxn67f4ahfswyjp8
;; ShareTechMono 13xbrc4d6bc1wjaybvbhnlsiv874sbkrjc7ai7310bv8z5v9h51s
;; SourceCodePro 0b048l525pig3wj7yr86710233xfnd7gpaigc04530b3ah013qw5
;; SpaceMono 04awas15c38vlzk6wzagv0p0sac4glfzzlp2jf8gbxdf11hcl1lg
;; Terminus 00j8glrgdc5w53k51bl8gilncw166h8i5yk9cbl5kbkpmn1h6y4q
;; Tinos 1zbv4lmv5chxd7ycvjv0d6fpddpll9r4b9w5jflrkdjlmyq74c5y
;; Ubuntu 1v3vc2nsl3a1yam2qf4rpiyd6wgpyn30n3a1izqn8sk2rm62zd33
;; UbuntuMono 1fg9abf421x3cwknh0chil4477gas289xp9s54m72ng9vx09jyxg
;; VictorMono 1ba50lhji0safw48v6r1xcxlgg92jgq4dklx9dk0garsfkhq4cib
;; iA-Writer 1yd9n558fd63q8317s8my3nqn7q598zwv631k4p3g83mp9wash9r
