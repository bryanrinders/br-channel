;;; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (channel-b packages gnome-xyz)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system copy)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define-public gruvbox-theme
  (let ((commit "f8fb23d9dd3a67d77c3b658d4abe862624e3cb6c"))
    (package
      (name "gruvbox-theme")
      (version "0")
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/bryos/gruvbox-gtk-theme")
               (commit commit)))
         (sha256
          (base32
           "1h8gg6c9mr8xwdy33w515l1xld37k7qjhb8mgjl1i0avz012bi4b"))
         (file-name (git-file-name name version))))
      (build-system copy-build-system)
      (arguments
       `(#:install-plan
         `(("./icons" "share/icons")
           ("./themes" "share/themes"))))
      (home-page "https://gitlab.com/bryos/gruvbox-gtk-theme")
      (synopsis "Gruvbox theme and icons for GTK.")
      (description "A very minimalistic, in terms of disk usage, Gruvbox GTK
theme for gtk-2.0 gtk-3.0 gtk-4.0.")
      (license license:gpl3))))
